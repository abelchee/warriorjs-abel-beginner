class Player {
  direction: 'backward';

  playTurn(warrior) {
    const health = warrior.health();
    let healthLost = 0;
    if (this.lastHealth) {
      healthLost = this.lastHealth - health;
    }
    this.lastHealth = health;

    if (warrior.feel(this.direction).isEmpty()) {
      if (health < 20 && (healthLost <= 0 || healthLost > 0 && this.fighting)) {
        warrior.rest();
      } else {
        warrior.walk();
      }
      this.fighting = false;
    }
    else if (warrior.feel(this.direction).isCaptive()) {
      this.fighting = false;
      warrior.rescue(this.direction);
    }
    else {
      warrior.attack(this.direction);
      this.fighting = true;
    }
  }
}
